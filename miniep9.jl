function multiplica(a, b)
	dima = size(a)
	dimb = size(b)
	if dima[2] != dimb[1]
		return -1
	end
	c = zeros(dima[1], dimb[2])
	for i in 1:dima[1]
		for j in 1:dimb[2]
			for k in 1:dima[2]
				c[i, j] = c[i, j] + a[i, k] * b[k, j]
			end
		end
	end
	return c
end

using Test

function test()
	@test matrix_pot([1 2 ; 3 4], 1) == [1 2; 3 4]
	@test matrix_pot([1 2 ; 3 4], 2) == [7.0 10.0; 15.0 22.0]
	@test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == [5.6428432e8 4.70888932e8 3.23583236e8 3.51858636e8;
 8.31242352e8 6.93529366e8 4.76618654e8 5.18192434e8;
 5.77003992e8 4.81472568e8 3.30793288e8 3.59676984e8;
 7.99037372e8 6.66708057e8 4.58121425e8 4.98126827e8]

	println("Fim dos testes matrix_pot")
end

#Parte 1

function matrix_pot(M,x)
	fixo = M
	while x > 1 
		M = multiplica(M,fixo)
		x = x - 1
	end
	return M
end

#Parte 2

function test2()
	@test matrix_pot_by_squaring([1 2 ; 3 4], 1) == [1 2; 3 4]
	@test matrix_pot_by_squaring([1 2 ; 3 4], 2) == [7.0 10.0; 15.0 22.0]
	@test matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == 
[5.6428432e8 4.70888932e8 3.23583236e8 3.51858636e8;
 8.31242352e8 6.93529366e8 4.76618654e8 5.18192434e8;
 5.77003992e8 4.81472568e8 3.30793288e8 3.59676984e8;
 7.99037372e8 6.66708057e8 4.58121425e8 4.98126827e8]

	println("Fim dos testes matrix_pot_by_squaring")
end

function matrix_pot_by_squaring(M,p)
	if p == 1
		return M
	end
	quadrado = multiplica(M,M)
	if p % 2 == 0
		return matrix_pot_by_squaring(quadrado,p/2)
	else
		return multiplica(matrix_pot_by_squaring(quadrado,(p-1)/2),M)
	end
end

